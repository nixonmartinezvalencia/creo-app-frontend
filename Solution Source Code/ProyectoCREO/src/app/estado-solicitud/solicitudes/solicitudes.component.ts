import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';
import { UserService } from 'src/app/users/service/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SolicitudCreditoService } from 'src/app/users/service/SolicitudCredito.service';
import { User } from 'src/app/users/model/user';
import { SolicitudCredito } from 'src/app/users/model/SolicitudCredito';

@Component({
  selector: 'app-solicitudes',
  templateUrl: './solicitudes.component.html',
  styleUrls: ['./solicitudes.component.css']
})
export class SolicitudesComponent implements OnInit {

  user: User = new User();
  currentUser: string;
  idUsuario: string = "";
  isLogin = false;
  numeroSolicitud: string = "";
  solicitudesCredito: SolicitudCredito[] = [];


  constructor(
    private tokenService: TokenService,
    private userService: UserService,
    private solicitudCreditoService: SolicitudCreditoService,
    private rutaActiva: ActivatedRoute,
    private router: Router

  ) { }

  ngOnInit(): void {
    this.numeroSolicitud = this.rutaActiva.snapshot.paramMap.get("id");

    this.currentUser = this.tokenService.getUserName();
    const helper = new JwtHelperService();

    if (!helper.isTokenExpired(this.tokenService.getToken())) {
      this.isLogin = true;


    }

    if (this.isLogin) {

      this.userService.detailByName(this.tokenService.getUserName())
        .subscribe(data => {


          this.user = data;

          this.solicitudCreditoService.getSolicitudesByUsuario(this.user.id).subscribe(data2 => {

            this.solicitudesCredito = data2;

          });



        });

    }



  }
}
