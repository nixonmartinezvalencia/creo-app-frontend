import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';
import { UserService } from 'src/app/users/service/user.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ActivatedRoute, Router } from '@angular/router';
import { SolicitudCreditoService } from 'src/app/users/service/SolicitudCredito.service';
import { SolicitudCredito } from 'src/app/users/model/SolicitudCredito';
import { Entrega } from 'src/app/users/model/Entrega';

@Component({
  selector: 'app-solicitud',
  templateUrl: './solicitud.component.html',
  styleUrls: ['./solicitud.component.css']
})
export class SolicitudComponent implements OnInit {


  currentUser: string;
  idUsuario: string = "";
  isLogin = false;
  numeroSolicitud: string = "";
  solicitudCredito: SolicitudCredito = new SolicitudCredito();
  entrega: Entrega = new Entrega();

  constructor(

    private tokenService: TokenService,
    private userService: UserService,
    private solicitudCreditoService: SolicitudCreditoService,
    private rutaActiva: ActivatedRoute,
    private router: Router

  ) { }

  ngOnInit(): void {
    this.numeroSolicitud = this.rutaActiva.snapshot.paramMap.get("id");

    this.currentUser = this.tokenService.getUserName();
    const helper = new JwtHelperService();

    if (!helper.isTokenExpired(this.tokenService.getToken())) {
      this.isLogin = true;


    }

    if (this.isLogin) {

      this.userService.detailByName(this.tokenService.getUserName())
        .subscribe(data => {

          this.idUsuario = data.id;

          this.solicitudCreditoService.getSolicitudByUsuarioAndId(this.idUsuario, this.numeroSolicitud)
            .subscribe(data2 => {
              this.solicitudCredito = data2;
              this.entrega = this.solicitudCredito.entrega;


            }


            );


        });

    }



  }

  goToEntrega() {

    this.router.navigate(['solicitudDeCredito', 'entrega',this.numeroSolicitud]);
  }

  listarSolicitudes() {

    this.router.navigate(['estadoSolicitud', 'solicitudes']);
  }


}
