import { Component, OnInit } from '@angular/core';
import { CreditoService } from 'src/app/users/service/Credito.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';
import { UserService } from 'src/app/users/service/user.service';
import { User } from 'src/app/users/model/user';
import { SolicitudCreditoService } from 'src/app/users/service/SolicitudCredito.service';
import { SolicitudCredito } from 'src/app/users/model/SolicitudCredito';

@Component({
  selector: 'app-estado',
  templateUrl: './estado.component.html',
  styleUrls: ['./estado.component.css']
})
export class EstadoComponent implements OnInit {


  currentUser: string;
  isLogin = false;
  id: string = "";
  user: User = new User();
  solicitudesCredito: SolicitudCredito[] = [];
  solicitudCredito: SolicitudCredito = new SolicitudCredito();

  isValidSolicitudCredito = false;

  constructor(
    private tokenService: TokenService,
    private userService: UserService,
    private creditoService: CreditoService,
    private solicitudCreditoService: SolicitudCreditoService,
    private rutaActiva: ActivatedRoute,
    private router: Router,


  ) { }

  ngOnInit() {

    // this.id = this.rutaActiva.snapshot.paramMap.get("id");
    this.currentUser = this.tokenService.getUserName();
    const helper = new JwtHelperService();

    if (!helper.isTokenExpired(this.tokenService.getToken())) {
      this.isLogin = true;


    }

    if (this.isLogin) {

      this.userService.detailByName(this.tokenService.getUserName())
        .subscribe(data => {

          this.user = data;

          this.solicitudCreditoService.getSolicitudesByUsuario(this.user.id).subscribe(data2 => {

            if (data2 != null) {

              this.solicitudesCredito = data2;

              this.solicitudCredito = this.solicitudesCredito[this.solicitudesCredito.length - 1];
              this.isValidSolicitudCredito = true;
            }



          });

        });
    }


  }



  generateReportPDF() {

    let mensaje = "";

    this.creditoService.generateReportPDF(this.user.id,this.solicitudCredito.numeroSolicitud).subscribe(data => {

      mensaje = data;
    }


    );

  }

  goToEntrega() {

    this.router.navigate(['solicitudDeCredito', 'entrega', this.solicitudCredito.numeroSolicitud]);
  }

}
