import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../service/user.service';
import { AuthService } from 'src/app/inicio/Security/_auth/auth.service';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { JwtHelperService } from '@auth0/angular-jwt';


@Component({
  selector: 'app-credenciales',
  templateUrl: './credenciales.component.html',
  styleUrls: ['./credenciales.component.css']
})
export class CredencialesComponent implements OnInit {

  dato: string[];

  user: User = new User();
  userForm: User = new User();
  newUser: User = new User();
  username: string;
  password: string;
  id: string = "";
  confirmPassword: string = "";
  isLogged = false;
  isLoginFail = false;
  errorMsg = '';


  constructor(private userService: UserService,
    private router: Router,
    private rutaActiva: ActivatedRoute,
    private authService: AuthService,
    private tokenService: TokenService,
    private spinnerService: NgxSpinnerService) { }

  ngOnInit() {

    const helper = new JwtHelperService();

    if (!helper.isTokenExpired(this.tokenService.getToken())) {

      this.isLogged = true;
      this.router.navigate(['inicio']);

    }

    this.id = this.rutaActiva.snapshot.paramMap.get("id");
    this.userService.detail(this.id).subscribe(data => {
      this.user = data;

    },
      err => {
        console.log(err);
      }
    );


  }

  validatePassword() {

    if (this.userForm.password == this.confirmPassword) {
      this.registrar();

    } else {


      this.errorMsg = "las contraseñas no coinciden";
    }
  }

  registrar() {

    this.spinnerService.show();


    this.username = this.userForm.username;
    this.password = this.userForm.password;

    this.userForm = this.user;
    this.userForm.username = this.username;
    this.userForm.password = this.password;


    this.userService.create(this.userForm).subscribe(us => {

      this.authService.login(this.userForm).subscribe(data => {
        this.tokenService.setToken(data.token);
        this.tokenService.setUserName(data.username);
        this.tokenService.setAuthorities(data.authorities);
        this.userService.detailByName(this.tokenService.getUserName())
          .subscribe(data => {

            if (data.enable) {

              this.isLogged = true;
              this.isLoginFail = false;

              window.location.reload();

            }
            else {
              this.isLogged = false;
              this.isLoginFail = true;
              this.spinnerService.hide();
              this.errorMsg = "El usuario se encuentra inactivo";
            }
          });
      },
        (err: any) => {
          this.spinnerService.hide();
          this.errorMsg = err.error.message;
          alert("error");
        }
      );



    });


  }


  mayuscula(): boolean {
    return this.validarCaracter(65, 90);
  }

  minuscula(): boolean {
    return this.validarCaracter(97, 122);
  }

  numero(): boolean {
    return this.validarCaracter(48, 57);
  }


  validarCaracter(rangoInicial: number, rangoFinal: number): boolean {

    this.dato = this.userForm.password.split("");
    for (let i = 0; i <= this.dato.length; i++) {

      if (this.userForm.password.charCodeAt(i) >= rangoInicial && this.userForm.password.charCodeAt(i) <= rangoFinal) {

        return true;
      }

    }
    return false;
  }


  passwordLength(): boolean {

    return this.userForm.password.length >= 8;

  }

  passwordIsEmpty(): boolean {

    return this.userForm.password.length == 0;
  }

  usernameIsEmpty(): boolean {

    return this.userForm.username.length == 0;
  }




  mostrarPassword1() {

    const pass1 = document.querySelector('#password');
    if (pass1.getAttribute('type') == 'password') {
      pass1.setAttribute('type', 'text');


    } else {


      pass1.setAttribute('type', 'password');
    }


  }

  mostrarPassword2() {

    const pass2 = document.querySelector('#confirmPassword');
    if (pass2.getAttribute('type') == 'password') {
      pass2.setAttribute('type', 'text');


    } else {

      pass2.setAttribute('type', 'password');
    }

  }

  userError() {

    this.userService.detailByName(this.userForm.username)
      .subscribe(data => {

        if (data !== null) {
          this.errorMsg = "el usuario ya existe en el sistema";

        } else {

          this.registrar();
        }


      });
  }
}
