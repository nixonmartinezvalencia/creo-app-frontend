import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { PersonalData } from '../model/PersonalData';
import { UserService } from 'src/app/users/service/user.service';
import { Validation } from '../model/Validation';
import { Router } from '@angular/router';

@Component({
  selector: 'app-autenticacion',
  templateUrl: './autenticacion.component.html',
  styleUrls: ['./autenticacion.component.css']
})
export class AutenticacionComponent implements OnInit {

  user: User = new User();
  personalData: PersonalData = new PersonalData();
  validation: Validation = new Validation();
  validationForm: Validation = new Validation();

  codigo :string;
  
 
  errorMsg = "";

  constructor(
    private userService: UserService, private router: Router
  ) { }

  ngOnInit(): void {

  }

  validarUsuario() {
    this.userService.detailByIdentificacion(this.personalData.identificacion).subscribe(data => {

      this.validation = data;
      if (this.validation.mensaje == 'debes registrarte') {
        this.router.navigate(['users', 'datosPersonales']);
      }

    });



  }

  validNumberPhone() {


    this.userService.detailByIdAndPhone(this.validation.user.personalData.identificacion, this.personalData.telefonoFijo).subscribe(data => {


      if (data.codigoVerificacion == '') {

        this.errorMsg = "numero incorrecto";
      } else {

        this.validation = data;
        this.errorMsg = "se enviara codigo";
        
      }

    });



  }

  validarCodigo() {

     this.codigo = this.validationForm.codigoVerificacion;


    if (this.codigo == this.validation.codigoVerificacion) {

    
      this.router.navigate(['users', 'datosPersonales',this.validation.user.id]);
    }

    else {

      this.errorMsg = "token incorrecto";
    }

  }

}
