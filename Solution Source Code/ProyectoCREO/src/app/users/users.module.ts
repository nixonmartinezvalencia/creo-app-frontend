import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { CredencialesComponent } from './credenciales/credenciales.component';
import { DatosPersonalesComponent } from './datos-personales/datos-personales.component';
import { FormsModule } from '@angular/forms';
import { AutenticacionComponent } from './autenticacion/autenticacion.component';


@NgModule({
  declarations: [

    CredencialesComponent,
    DatosPersonalesComponent,
    AutenticacionComponent

    ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule

  ]
})
export class UsersModule { }
