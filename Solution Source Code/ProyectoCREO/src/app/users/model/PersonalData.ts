

export class PersonalData {

    ciudad: string = "";
    departamento: string = "";
    nombre: string = "";
    apellidos: string = "";
    identificacion: string = "";
    tipoIdentificacion: string = "";
    personasACargo: string = "";
    direccion: string = "";
    tipoVivienda: string = "";
    telefonoFijo: string = "";
    numeroMovil: string = "";
    fechaNacimiento: string = "";
    tipoCarro: string = "";
    placa: string = "";
    modelo: string = "";
    marca: string = "";
    cuentaBancaria: string = "";
    entidadBancaria: string = "";
    tipoDeCuenta: string = "";
    correo: string = "";

}