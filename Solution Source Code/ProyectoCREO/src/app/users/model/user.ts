
import { Role } from './roles';
import { PersonalData } from './PersonalData';
import { WorkingInformation } from './WorkingInformation';
import { References } from './References';
import { Codebtors } from './Codebtors';
import { Entrega } from './Entrega';

export class User {
    id: string;
    username: string = "";
    password: string = "";
    enable: boolean = true;
    rol: Role = new Role();

    personalData: PersonalData;
    workingInformation: WorkingInformation;
    references: References[];
    codebtors: Codebtors[];
    entrega: Entrega;

}