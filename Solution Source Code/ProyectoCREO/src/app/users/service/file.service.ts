import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/users/model/user';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';
import { Entrega } from '../model/Entrega';
import { SolicitudCredito } from '../model/SolicitudCredito';

const token = new TokenService().getToken();
const header = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

@Injectable({
  providedIn: 'root'
})
export class FileService {

  url: string = `${environment.Urllocalhost}/solicitudCredito`;

  constructor(private httpClient: HttpClient) { }

  public descargarDocumento(id: string, numeroSolicitud: string): Observable<Blob> {

    return this.httpClient.get(`${this.url}/descargarDocumento/${id}/${numeroSolicitud}`, {
      responseType: "blob"
    });

  }

  public descargarDesprendible(id: string, numeroSolicitud: string): Observable<Blob> {

    return this.httpClient.get(`${this.url}/descargarDesprendible/${id}/${numeroSolicitud}`, {
      responseType: "blob"
    });

  }

  public descargarCartaLaboral(id: string, numeroSolicitud: string): Observable<Blob> {

    return this.httpClient.get(`${this.url}/descargarCartaLaboral/${id}/${numeroSolicitud}`, {
      responseType: "blob"
    });

  }

  public uploadFiles(id: string, file: File, file2: File, file3: File): Observable<SolicitudCredito> {

    const formdata: FormData = new FormData();

    formdata.append('documento', file);
    formdata.append('desprendibleNomina', file2);
    formdata.append('cartaLaboral', file3);

    return this.httpClient.post<SolicitudCredito>(`${this.url}/uploadFiles/${id}`, formdata);

  }

}
