import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/users/model/user';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';
import { Validation } from '../model/Validation';
import { References } from '../model/References';
import { Codebtors } from '../model/Codebtors';
import { WorkingInformation } from '../model/WorkingInformation';
import { PersonalData } from '../model/PersonalData';
import { Entrega } from '../model/Entrega';
import { SolicitudCredito } from '../model/SolicitudCredito';
import { SolicitudesComponent } from 'src/app/estado-solicitud/solicitudes/solicitudes.component';

const token = new TokenService().getToken();
const header = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

@Injectable({
  providedIn: 'root'
})
export class SolicitudCreditoService {

  url: string = `${environment.Urllocalhost}/solicitudCredito`;

  constructor(private httpClient: HttpClient, private tokenService: TokenService) { }


  public generateReportPDF(id: string): Observable<string> {
    return this.httpClient.get<string>(`${this.url}/generateReportPDF/${id}`, header);
  }

  public getSolicitudByUsuarioAndId(idUsuario: string, numeroSolicitud: string): Observable<SolicitudCredito> {
    return this.httpClient.get<SolicitudCredito>(`${this.url}/solicitud/${idUsuario}/${numeroSolicitud}`, header);
  }

  public getSolicitudesByUsuario(idUsuario: string): Observable<SolicitudCredito[]> {
    return this.httpClient.get<SolicitudCredito[]>(`${this.url}/solicitudes/${idUsuario}`, header);
  }

  public crearSolicitud(solicitudCredito: SolicitudCredito): Observable<SolicitudCredito> {
    return this.httpClient.post<SolicitudCredito>(`${this.url}/crearSolicitud`, solicitudCredito, header);
  }

  

}
