import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/users/model/user';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';
import { Validation } from '../model/Validation';
import { References } from '../model/References';
import { Codebtors } from '../model/Codebtors';
import { WorkingInformation } from '../model/WorkingInformation';
import { PersonalData } from '../model/PersonalData';
import { Entrega } from '../model/Entrega';

const token = new TokenService().getToken();
const header = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url: string = `${environment.Urllocalhost}/customers`;

  constructor(private httpClient: HttpClient, private tokenService: TokenService) { }

  public list(userName: string): Observable<User[]> {
    return this.httpClient.get<User[]>(`${environment.apiUrl}/user/rol/${userName}`, header);
  }

  public listRevisor(): Observable<User[]> {
    return this.httpClient.get<User[]>(`${environment.apiUrl}/users/rol`, header);
  }


  public getSupervisor(idInv: string): Observable<User> {
    return this.httpClient.get<User>(`${environment.apiUrl}/user/role/${idInv}`, header);
  }


  public detailByName(name: string): Observable<User> {

    return this.httpClient.get<User>(`${environment.apiUrl}/user/name/${name}`, header);
  }

  public create(user: User) {
    return this.httpClient.post<User>(`${environment.apiUrl}/auth/user`, user, header);
  }

  public edit(user: User, id: string): Observable<User> {
    return this.httpClient.put<User>(`${environment.apiUrl}/user/${id}`, user, header);
  }

  public detail(id: string): Observable<User> {
    return this.httpClient.get<User>(`${environment.apiUrl}/auth/user/${id}`, header);
  }

  public delete(id: string): Observable<any> {
    return this.httpClient.delete<any>(`${environment.apiUrl}/user/${id}`, header);
  }

  public changeState(id: string): Observable<any> {
    return this.httpClient.put<any>(`${environment.apiUrl}/user/state/${id}`, header);
  }




  public detailByIdentificacion(id: string): Observable<Validation> {
    return this.httpClient.get<Validation>(`${this.url}/auth/identificacion/${id}`, header);
  }

  public detailByIdAndPhone(id: string, numberPhone: string): Observable<Validation> {
    return this.httpClient.get<Validation>(`${this.url}/auth/phone/${id}/${numberPhone}`, header);
  }

  public detailByIdAndReference(id: string, idReference: string): Observable<References> {
    return this.httpClient.get<References>(`${this.url}/currentReference/${id}/${idReference}`, header);
  }

  public detailByIdAndCodebtor(id: string, idCodebtor: string): Observable<Codebtors> {
    return this.httpClient.get<Codebtors>(`${this.url}/currentCodebtor/${id}/${idCodebtor}`, header);
  }

  public getReferenceById(id: string, idReference: string): Observable<User> {
    return this.httpClient.get<User>(`${this.url}/reference/${id}/${idReference}`, header);
  }
  public getCodebtorById(id: string, idCodebtor: string): Observable<User> {
    return this.httpClient.get<User>(`${this.url}/codebtor/${id}/${idCodebtor}`, header);
  }


  public updatePersonalData(personalData: PersonalData, id: string): Observable<PersonalData> {
    return this.httpClient.post<PersonalData>(`${this.url}/user/personalData/${id}`, personalData, header);
  }
  public updateWorkingInformation(workingInformation: WorkingInformation, id: string): Observable<WorkingInformation> {
    return this.httpClient.put<WorkingInformation>(`${this.url}/user/workingInformation/${id}`, workingInformation, header);
  }

  public updateReferences(references: References[], id: string): Observable<References[]> {
    return this.httpClient.put<References[]>(`${this.url}/user/references/${id}`, references, header);
  }

  public updateCodebtors(codebtors: Codebtors[], id: string): Observable<Codebtors[]> {
    return this.httpClient.put<Codebtors[]>(`${this.url}/user/codebtors/${id}`, codebtors, header);
  }

  public updateEntrega(entrega: Entrega, id: string): Observable<Entrega> {
    return this.httpClient.post<Entrega>(`${this.url}/user/entrega/${id}`, entrega, header);
  }

}
