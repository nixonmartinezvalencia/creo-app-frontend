import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './inicio/inicio/inicio.component';
import { LoginComponent } from './inicio/login/login.component';

const routes: Routes = [

  { path: 'inicio', component: InicioComponent },
  { path: 'login', component: LoginComponent },

  { 
    path: 'users' ,
   loadChildren: () => import('./users/users.module').then(mod => mod.UsersModule)
  
  },

  { 
    path: 'solicitudDeCredito' ,
   loadChildren: () => import('./solicitud-de-credito/solicitud-de-credito.module').then(mod => mod.SolicitudDeCreditoModule)
  
  },
  { 
    path: 'estadoSolicitud' ,
   loadChildren: () => import('./estado-solicitud/estado-solicitud.module').then(mod => mod.EstadoSolicitudModule)
  
  },

  { path: '**', redirectTo: '/inicio', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
  