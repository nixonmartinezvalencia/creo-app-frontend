import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-update-tabs',
  templateUrl: './update-tabs.component.html',
  styleUrls: ['./update-tabs.component.css']
})
export class UpdateTabsComponent implements OnInit {

  numeroSolicitud: string = "";

  isPersonalData: boolean = false;
  isWorkingInformation: boolean = false;
  isReferences: boolean = false;
  isCodebtors: boolean = false;


  constructor(
    private rutaActiva: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.numeroSolicitud = this.rutaActiva.snapshot.paramMap.get("numeroSolicitud");


    if (this.numeroSolicitud != null) {


      if (this.rutaActiva.toString().includes('datosPersonales')) {
        this.isPersonalData = true;

      }
      if (this.rutaActiva.toString().includes('informacionLaboral')) {
        this.isWorkingInformation = true;

      }
      if (this.rutaActiva.toString().includes('referencias')) {
        this.isReferences = true;

      }
      if (this.rutaActiva.toString().includes('codeudores')) {
        this.isCodebtors = true;

      }

    }


  }



}
