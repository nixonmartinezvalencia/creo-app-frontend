import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/users/service/user.service';

import { NgxSpinnerService } from 'ngx-spinner'
import { AuthService } from 'src/app/inicio/Security/_auth/auth.service';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';
import { User } from 'src/app/users/model/user';
import { JwtHelperService } from '@auth0/angular-jwt';

const IS_LOGGED = 'falso';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  //form: any = {};
  user: User = new User();
  isLogged = false;
  isLoginFail = false;
  currentUser: string;
  roles: string[] = [];
  errorMsg = '';
  rutaLogo;

  constructor(private router: Router,
    private authService: AuthService,
    private tokenService: TokenService,
    private spinnerService: NgxSpinnerService,
    private userService: UserService) { }

  ngOnInit() {


    this.currentUser = this.tokenService.getUserName();
    const helper = new JwtHelperService();

    if (!helper.isTokenExpired(this.tokenService.getToken())) {
      this.isLogged = true;

      this.router.navigate(['inicio']);


    }

  }

  spinner() {
    this.spinnerService.show();
    setTimeout(() => {
      this.spinnerService.hide();
    }, 1000);
  }

  onLogin(): void {
    this.spinnerService.show();
    this.authService.login(this.user).subscribe(data => {
      this.tokenService.setToken(data.token);
      this.tokenService.setUserName(data.username);
      this.tokenService.setAuthorities(data.authorities);
      this.roles = this.tokenService.getAuthorities();
      this.userService.detailByName(this.tokenService.getUserName())
        .subscribe(data => {
          if (data.enable) {

            this.isLogged = true;
            this.isLoginFail = false;
            window.sessionStorage.setItem(IS_LOGGED, 'verdadero');
            window.location.reload();



          } else {
            this.spinnerService.hide();
            this.isLogged = false;
            this.isLoginFail = true;

            window.sessionStorage.setItem(IS_LOGGED, 'falso');
            this.errorMsg = "El usuario se encuentra inactivo";
          }
        }, (err: any) => {

          window.sessionStorage.setItem(IS_LOGGED, 'falso');
          this.spinnerService.hide();
        });
    },
      (err: any) => {
        this.spinnerService.hide();
        this.isLogged = false;
        this.isLoginFail = true;

        window.sessionStorage.setItem(IS_LOGGED, 'falso');
        this.errorMsg = err.error.message;
      }
    );


  }

  newAccess(username) {
    this.userService.detailByName(username)
      .subscribe(data => {

        this.userService.edit(data, data.id)
          .subscribe(dat => {

          },
            (err: any) => {
              console.log(err);
            }
          );
      },
        (err: any) => {
          console.log(err);
        }
      );
  }

  registrar() {
    this.router.navigate(['users', 'datosPersonales']);

  }

  recordarUsuario() {

    this.router.navigate(['users', 'autenticacion']);
  }

}
