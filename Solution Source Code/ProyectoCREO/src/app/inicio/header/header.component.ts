import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TokenService } from '../Security/_auth/token.service';
import { UserService } from 'src/app/users/service/user.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from 'src/app/users/model/user';
import { SolicitudCreditoService } from 'src/app/users/service/SolicitudCredito.service';

const HOME = 'true';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isLogin = false;
  currentUser: string;
  isHome = true;
  thereIsSolicitud = false;

  user: User = new User();

  constructor(private router: Router,
    private tokenService: TokenService,
    private userService: UserService,
    private solicitudCreditoService: SolicitudCreditoService
  ) { }


  logOut(): void {
    this.isLogin = false;
    this.isHome = true
    this.tokenService.logOut();


    this.router.navigate(['inicio']);


  }

  ingresar() {
    this.isHome = false;
    this.router.navigate(['login']);


  }

  ngOnInit(): void {


    this.currentUser = this.tokenService.getUserName();
    const helper = new JwtHelperService();

    if (!helper.isTokenExpired(this.tokenService.getToken())) {
      this.isLogin = true;
      this.isHome = false;
      this.getCurrentUser(this.currentUser);


    }



    if (!this.isLogin) {
      this.isHome = true;
    }
    else {

      this.userService.detailByName(this.tokenService.getUserName())
        .subscribe(data => {

          this.user = data;

        });
    }

    if (window.location.pathname.startsWith('/login') || window.location.pathname.startsWith('/users')) {
      this.isHome = false;
    }





  }

  getCurrentUser(userName: string) {
    this.userService.detailByName(userName)
      .subscribe(data => {

      },
        (err: any) => {
          console.log(err);
        }
      );
  }


  registrar() {
    this.isHome = false;
    this.router.navigate(['users', 'datosPersonales']);
  }

  solicitarCredito() {
    this.router.navigate(['solicitudDeCredito', 'creditos']);

  }

  estadoSolicitud() {


    this.solicitudCreditoService.getSolicitudesByUsuario(this.user.id).subscribe(data2 => {

      if (data2[0] != null) {

        this.thereIsSolicitud = true;
        this.router.navigate(['estadoSolicitud', 'estado']);
      } 



    });



  }


}
