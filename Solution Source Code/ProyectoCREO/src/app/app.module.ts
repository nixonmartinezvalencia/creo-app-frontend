import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioModule } from './inicio/inicio.module';
import { HeaderComponent } from './inicio/header/header.component';
import { interceptorProvider } from './inicio/Security/_interceptors/interceptor.service';
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatosPersonalesComponent } from './solicitud-de-credito/datos-personales/datos-personales.component';
import { InformacionLaboralComponent } from './solicitud-de-credito/informacion-laboral/informacion-laboral.component';
import { ReferenciasComponent } from './solicitud-de-credito/referencias/referencias.component';
import { CodeudoresComponent } from './solicitud-de-credito/codeudores/codeudores.component';
import { EntregaComponent } from './solicitud-de-credito/entrega/entrega.component';
import { UpdateTabsComponent } from './inicio/update-tabs/update-tabs.component';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    UpdateTabsComponent,
    DatosPersonalesComponent,
    InformacionLaboralComponent,
    ReferenciasComponent,
    CodeudoresComponent,EntregaComponent
    
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    InicioModule,
    HttpClientModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
  ],
  providers: [interceptorProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
