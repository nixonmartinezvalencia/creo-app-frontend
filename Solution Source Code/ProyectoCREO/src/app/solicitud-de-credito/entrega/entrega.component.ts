
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as fileSaver from 'file-saver';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/users/model/user';
import { Role } from 'src/app/users/model/roles';
import { Permission } from 'src/app/users/model/permission';
import { UserService } from 'src/app/users/service/user.service';
import { Entrega } from 'src/app/users/model/Entrega';
import { FileService } from 'src/app/users/service/file.service';
import { DatosPersonalesComponent } from 'src/app/users/datos-personales/datos-personales.component';
import { SolicitudCredito } from 'src/app/users/model/SolicitudCredito';
import { SolicitudCreditoService } from 'src/app/users/service/SolicitudCredito.service';

declare var jQuery: any;


@Component({
  selector: 'app-entrega',
  templateUrl: './entrega.component.html',
  styleUrls: ['./entrega.component.css']
})
export class EntregaComponent implements OnInit {


  descargar: boolean = false;
  numeroSolicitud: string = "";
  isUpdate: boolean = false;
  isValidEntrega: boolean = false;

  isDownloand: boolean = false;

  archivo: Blob;
  idSolicitud: string = "";
  selectedFiles: FileList;
  selectedFiles2: FileList;
  selectedFiles3: FileList;
  selectedFilesAnexo: Array<FileList> = [];
  currentFile: File;
  currentFile2: File;
  currentFile3: File;

  user: User = new User();
  entrega: Entrega = new Entrega();

  solicitudCredito: SolicitudCredito = new SolicitudCredito();

  rol: Role = new Role();
  permissions: Permission[] = [];
  errorMsg = '';
  id: string = "";
  wrongForm = false;
  form: any = {};
  currentUser: string;
  isLogin = false;

  estadoArchivos: boolean;


  constructor(private tokenService: TokenService,
    private userService: UserService,
    private solicitudCreditoService: SolicitudCreditoService,
    private rutaActiva: ActivatedRoute,
    private router: Router,
    private fileService: FileService
  ) { }

  ngOnInit() {

    this.numeroSolicitud = this.rutaActiva.snapshot.paramMap.get("numeroSolicitud");

    if (this.numeroSolicitud != null) {
      this.isUpdate = true;

    }

    this.currentUser = this.tokenService.getUserName();
    const helper = new JwtHelperService();

    if (!helper.isTokenExpired(this.tokenService.getToken())) {
      this.isLogin = true;


    }

    if (this.isLogin) {

      this.userService.detailByName(this.tokenService.getUserName())
        .subscribe(data => {

          this.user = data;

          if (this.user.entrega !== null) {

            //this.entrega = this.user.entrega;
            // this.entrega = this.solicitudCredito.entrega;

          }

          if (this.numeroSolicitud != null) {

            this.solicitudCreditoService.getSolicitudByUsuarioAndId(this.user.id, this.numeroSolicitud)
              .subscribe(data2 => {

                this.entrega = data2.entrega;

              });

          }



        });
    }






  }




  registrar() {

    this.estadoArchivos = (this.selectedFiles != null) && (this.selectedFiles2 != null) && (this.selectedFiles3 != null);
    this.isValidEntrega = (this.entrega.documento != null) && (this.entrega.cartaLaboral != null) && (this.entrega.desprendibleNomina != null);

    if (this.estadoArchivos && this.validarFormulario()) {
      this.upload();
    }

    if (!this.estadoArchivos && this.isValidEntrega && this.validarFormulario()) {

      if (this.numeroSolicitud != null) {

        this.solicitudCredito.numeroSolicitud = this.numeroSolicitud;
      }

      this.solicitudCredito.entrega = this.entrega;
      this.solicitudCredito.idUsuario = this.user.id;

      this.solicitudCreditoService.crearSolicitud(this.solicitudCredito).subscribe(data2 => {

        if (this.numeroSolicitud != null) {
          this.router.navigate(['solicitudDeCredito', 'datosPersonales', this.numeroSolicitud]);

        } else {

          this.router.navigate(['solicitudDeCredito', 'confirmacion', data2.numeroSolicitud]);
        }



      });

    }

  }

  public upload() {

    this.estadoArchivos = (this.selectedFiles != null) && (this.selectedFiles2 != null) && (this.selectedFiles3 != null);

    if (this.estadoArchivos) {

      this.currentFile = this.selectedFiles.item(0);
      this.currentFile2 = this.selectedFiles2.item(0);
      this.currentFile3 = this.selectedFiles3.item(0);



      this.fileService.uploadFiles(this.user.id, this.currentFile, this.currentFile2, this.currentFile3).subscribe(data => {


        this.solicitudCredito = data;

        if (this.numeroSolicitud != null) {

          this.solicitudCredito.numeroSolicitud = this.numeroSolicitud;
        }


        this.solicitudCredito.entrega.plazo = this.entrega.plazo;
        this.solicitudCredito.entrega.valorSolicitado = this.entrega.valorSolicitado;
        this.solicitudCredito.idUsuario = this.user.id;

        this.solicitudCreditoService.crearSolicitud(this.solicitudCredito).subscribe(data2 => {

          if (this.numeroSolicitud != null) {
            this.router.navigate(['solicitudDeCredito', 'datosPersonales', this.numeroSolicitud]);

          } else {

            this.router.navigate(['solicitudDeCredito', 'confirmacion', data2.numeroSolicitud]);
          }

        });

      });

    }

  }






  onFileChange(e) {
    console.log('file change', e);

    this.selectedFiles = e.target.files;

    var filename = e.target.files.item(0).name;
    const label1 = document.querySelector('[for="documento"]');
    label1.innerHTML = filename;


  }

  onFileChange2(e) {
    console.log('file change', e);

    this.selectedFiles2 = e.target.files

    var filename = e.target.files.item(0).name;
    const label1 = document.querySelector('[for="desprendibleNomina"]');
    label1.innerHTML = filename;
  }

  onFileChange3(e) {
    console.log('file change', e);

    this.selectedFiles3 = e.target.files

    var filename = e.target.files.item(0).name;
    const label1 = document.querySelector('[for="cartaLaboral"]');
    label1.innerHTML = filename;
  }


  descargarDocumento() {

    this.vaciarFormulario();

    this.fileService.descargarDocumento(this.user.id, this.numeroSolicitud).subscribe(data => {

      const blob = new Blob([data], { type: 'application/octet-stream' });
      fileSaver.saveAs(blob, "documento.pdf");


    });

  }

  descargarDesprendible() {

    this.vaciarFormulario();

    this.fileService.descargarDesprendible(this.user.id, this.numeroSolicitud).subscribe(event => {

      const blob = new Blob([event], { type: 'application/octet-stream' });
      fileSaver.saveAs(blob, "desprendible.pdf");


    });

  }

  descargarCartaLaboral() {

    this.vaciarFormulario();

    this.fileService.descargarCartaLaboral(this.user.id, this.numeroSolicitud).subscribe(data => {


      const blob = new Blob([data], { type: 'application/octet-stream' });
      window['saveAs'](blob);


    });



  }

  validarFormulario(): boolean {
    return this.entrega.plazo != "" &&
      this.entrega.valorSolicitado != "";
  }
  vaciarFormulario() {
    this.entrega.plazo = "";
    this.entrega.valorSolicitado = "";
  }

}