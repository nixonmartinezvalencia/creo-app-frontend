import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';

import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/users/model/user';

import { Role } from 'src/app/users/model/roles';
import { Permission } from 'src/app/users/model/permission';
import { UserService } from 'src/app/users/service/user.service';
import { Codebtors } from 'src/app/users/model/Codebtors';
import { References } from 'src/app/users/model/References';
import { Utility } from 'src/app/users/model/Utility';


declare var jQuery: any;

@Component({
  selector: 'app-codeudores',
  templateUrl: './codeudores.component.html',
  styleUrls: ['./codeudores.component.css']
})
export class CodeudoresComponent implements OnInit {

  @ViewChild('createModal', { static: false }) createModal: ElementRef;

  numeroSolicitud: string = "";
  isUpdate: boolean = false;

  utility: Utility = new Utility();
  tiposDeDocumento: string[] = this.utility.tiposDeDocumento;
  user: User = new User();

  codebtors: Codebtors[] = [];
  codebtor: Codebtors = new Codebtors();
  codebtorForm: Codebtors = new Codebtors();

  idCodebtor: string = "";

  rol: Role = new Role();
  permissions: Permission[] = [];
  errorMsg = '';
  isError: boolean = false;
  id: string = "";
  wrongForm = false;
  form: any = {};
  currentUser: string;
  isLogin = false;

  constructor(private tokenService: TokenService,
    private userService: UserService,
    private rutaActiva: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {

    this.numeroSolicitud = this.rutaActiva.snapshot.paramMap.get("numeroSolicitud");

    if (this.numeroSolicitud) {
      this.isUpdate = true;
    }

    this.currentUser = this.tokenService.getUserName();
    const helper = new JwtHelperService();

    if (!helper.isTokenExpired(this.tokenService.getToken())) {
      this.isLogin = true;


    }

    if (this.isLogin) {

      this.userService.detailByName(this.tokenService.getUserName())
        .subscribe(data => {

          this.user = data;

          if (this.user.codebtors !== null) {

            this.codebtors = this.user.codebtors;
          }


        });
    }


  }


  adjuntar() {



    this.userService.getCodebtorById(this.user.id, this.codebtor.identificacion)
      .subscribe(data => {

        if (data != null) {
          this.isError = true;
          this.errorMsg = 'El numero de identificacion para esta referencia ya existe';


        } else {

          this.codebtors.push(this.codebtor);
          this.userService.updateCodebtors(this.codebtors, this.user.id).subscribe(data => {

            this.codebtors = data;

          });

          this.codebtor = new Codebtors();

        }

      });



  }


  continuar() {
    if (this.numeroSolicitud != null) {

      this.router.navigate(['solicitudDeCredito', 'confirmacion', this.numeroSolicitud]);

    } else {



      if (this.codebtors.length < 2) {

        this.isError = true;
        this.errorMsg = "Ingrese minimo dos codeudores";

      } else {
        this.router.navigate(['solicitudDeCredito', 'entrega']);

      }


    }


  }

  logOut(): void {

    this.tokenService.logOut();
    this.router.navigate(['inicio']);

  }






  currentCodebtor(id: string, identificacionCodebtor: string) {

    this.userService.detailByIdAndCodebtor(id, identificacionCodebtor).subscribe(data => {

      this.codebtorForm = data;
      this.idCodebtor = identificacionCodebtor;
    },
      err => {
        console.log(err);
      });

  }



  onUpdate(codebtorForm: Codebtors) {

    let referencesLength = this.codebtors.length;


    this.codebtors.forEach(element => {

      if (element.identificacion == codebtorForm.identificacion) {

        let index = this.codebtors.indexOf(element);

        if (index !== -1) {
          this.codebtors.splice(index, 1);

        }


      }
    });

    this.codebtors.push(codebtorForm);


    if (referencesLength < this.codebtors.length) {

      this.codebtors.forEach(element => {
        if (element.identificacion == this.idCodebtor) {

          let index = this.codebtors.indexOf(element);

          if (index !== -1) {

            this.codebtors.splice(index, 1);

          }
        }

      });

    }


    this.userService.updateCodebtors(this.codebtors, this.user.id).subscribe(data => {

      this.codebtors = data;

    });

    jQuery(this.createModal.nativeElement).modal('hide');

  }



}



