import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';

import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/users/model/user';
import { Role } from 'src/app/users/model/roles';
import { Permission } from 'src/app/users/model/permission';
import { UserService } from 'src/app/users/service/user.service';
import { References } from 'src/app/users/model/References';
import { Utility } from 'src/app/users/model/Utility';
import { IfStmt } from '@angular/compiler';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';


declare var jQuery: any;

@Component({
  selector: 'app-referencias',
  templateUrl: './referencias.component.html',
  styleUrls: ['./referencias.component.css']
})
export class ReferenciasComponent implements OnInit {

  @ViewChild('createModal', { static: false }) createModal: ElementRef;


  utility: Utility = new Utility();

  numeroSolicitud: string = "";
  isUpdate: boolean = false;

  tiposDeDocumento: string[] = this.utility.tiposDeDocumento;
  tiposDeReferencia: string[] = this.utility.tiposDeReferencia;

  user: User = new User();

  references: References[] = [];
  reference: References = new References();
  referenceForm: References = new References();

  currentReference: References = new References();
  idReference: string = "";
  rol: Role = new Role();
  permissions: Permission[] = [];
  errorMsg = '';
  isError: boolean = false;
  id: string = "";
  wrongForm = false;
  form: any = {};
  currentUser: string;
  isLogin = false;



  constructor(private tokenService: TokenService,
    private userService: UserService,
    private router: Router,
    private rutaActiva: ActivatedRoute) { }

  ngOnInit() {
    this.numeroSolicitud = this.rutaActiva.snapshot.paramMap.get("numeroSolicitud");

    if (this.numeroSolicitud) {
      this.isUpdate = true;
    }

    this.currentUser = this.tokenService.getUserName();
    const helper = new JwtHelperService();

    if (!helper.isTokenExpired(this.tokenService.getToken())) {
      this.isLogin = true;


    }

    if (this.isLogin) {

      this.userService.detailByName(this.tokenService.getUserName())
        .subscribe(data => {

          this.user = data;

          if (this.user.references !== null) {

            this.references = this.user.references;
          }


        });
    }



  }

  adjuntar() {

    this.userService.getReferenceById(this.user.id, this.reference.identificacion)
      .subscribe(data => {

        if (data != null) {
          this.isError = true;
          this.errorMsg = 'El numero de identificacion para esta referencia ya existe';


        } else {

          this.references.push(this.reference);
          this.userService.updateReferences(this.references, this.user.id).subscribe(data => {

            this.references = data;

          });
          this.reference = new References();


        }

      });





  }


  continuar() {

    if (this.numeroSolicitud != null) {
      this.router.navigate(['solicitudDeCredito', 'codeudores', this.numeroSolicitud]);

    } else {

      if (this.references.length < 2) {

        this.isError = true;

        this.errorMsg = "Ingrese minimo dos referencias";

      } else {
        this.router.navigate(['solicitudDeCredito', 'codeudores']);
      }


    }



  }

  logOut(): void {

    this.tokenService.logOut();
    this.router.navigate(['inicio']);

  }


  getCurrentReference(id: string, identificacionReference: string) {

    this.userService.detailByIdAndReference(id, identificacionReference).subscribe(data => {

      this.referenceForm = data;
      this.idReference = identificacionReference;
    },
      err => {
        console.log(err);
      });

  }

  onUpdate(referenceForm: References) {

    let referencesLength = this.references.length;


    this.references.forEach(element => {

      if ((element.identificacion == referenceForm.identificacion)) {

        let index = this.references.indexOf(element);


        if (index !== -1) {

          this.references.splice(index, 1);

        }


      }

    });

    this.references.push(referenceForm);

    if (referencesLength < this.references.length) {

      this.references.forEach(element => {
        if (element.identificacion == this.idReference) {


          let index = this.references.indexOf(element);


          if (index !== -1) {

            this.references.splice(index, 1);

          }
        }

      });

    }

    this.userService.updateReferences(this.references, this.user.id).subscribe(data => {

      this.references = data;

    });

    jQuery(this.createModal.nativeElement).modal('hide');

  }




}



